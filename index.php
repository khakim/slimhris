<?php
$url_data     = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$url_data    .= "://".$_SERVER['HTTP_HOST'];
$url_data    .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

?>

<!DOCTYPE html>
<html>
    <head>
		<title>Compare Master Karyawan DBF vs Mysql</title>
        <script type="text/javascript" src="js/jsondiffpatch.min.js"></script>
        <script type="text/javascript" src="js/jsondiffpatch-formatters.min.js"></script>
        <link rel="stylesheet" href="js/html.css" type="text/css" />
        <link rel="stylesheet" href="js/annotated.css" type="text/css" />
    </head>
    <body>
	<div style="font-family:verdana;"> Legend &raquo; Warna Merah : DBF , Warna Hijau : Mysql </div>
    <div style="font-family:verdana;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &raquo; Warna Merah Coret: DBF Tidak ada/tdk Sama , Warna Hijau Coret: Mysql Tidak ada/tdk Sama</div>
	<div style="font-family:verdana;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &raquo; Data yg ditampilkan adalah data yg berbeda saja. </div>
	<div style="font-family:verdana;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &raquo; Index Warna Hitam : Data Baru di Mysql/DBF. </div>
<div style="font-family:verdana;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &raquo; Sync Data: <a href="<?php echo $url_data; ?>vendor/slim/slim/compare/223979163e6282261ab373ad748c94ed">PANDAAN</a>.
<a href="<?php echo $url_data; ?>vendor/slim/slim/compare/grs/223979163e6282261ab373ad748c94ed">GRESIK</a>. 
<a href="<?php echo $url_data; ?>vendor/slim/slim/compare/pkl/223979163e6282261ab373ad748c94ed">PEKALONGAN</a> 
Penjelasan : <a href="#penjelasan_visual">Visual PDN</a> <a href="#penjelasan_visual_gresik">Visual GRSK</a> <a href="#penjelasan_visual_pekalongan">Visual PKL</a>
</div>


		<div id='penjelasan_visual'>Penjelasan Visual PDN :  </div>
		<div id="visual"></div>
        <hr/>
		
        <div id="annotated"></div>

		<div id='penjelasan_visual_gresik'>Penjelasan Visual GRS :  </div>
		<div id="visual_gresik"></div>
        <hr/>

		<div id='penjelasan_visual_pekalongan'>Penjelasan Visual PKL :  </div>
		<div id="visual_pkl"></div>
        <hr/>


        <script>
        
		function Get(yourUrl){
			var Httpreq = new XMLHttpRequest(); // a new request
			Httpreq.open("GET",yourUrl,false);
			Httpreq.send(null);
			return Httpreq.responseText;          

   		 }
		
		var json_obj = JSON.parse(Get('<?php echo $url_data; ?>vendor/slim/slim/dbfkry/223979163e6282261ab373ad748c94ed'));
		var json_obj2 = JSON.parse(Get('<?php echo $url_data; ?>vendor/slim/slim/mykry/223979163e6282261ab373ad748c94ed'));
		
		var json_obj3 = JSON.parse(Get('<?php echo $url_data; ?>vendor/slim/slim/dbfkry/grs/223979163e6282261ab373ad748c94ed'));
		var json_obj4 = JSON.parse(Get('<?php echo $url_data; ?>vendor/slim/slim/mykry/grs/223979163e6282261ab373ad748c94ed'));
		
		var json_obj5 = JSON.parse(Get('<?php echo $url_data; ?>vendor/slim/slim/dbfkry/pkl/223979163e6282261ab373ad748c94ed'));
		var json_obj6 = JSON.parse(Get('<?php echo $url_data; ?>vendor/slim/slim/mykry/pkl/223979163e6282261ab373ad748c94ed'));




            var delta = jsondiffpatch.diff(json_obj, json_obj2);
            var reverseDelta = jsondiffpatch.reverse(delta);
			
			var delta_gresik = jsondiffpatch.diff(json_obj3, json_obj4);
            var reverseDelta_gresik = jsondiffpatch.reverse(delta_gresik);

			var delta_pkl = jsondiffpatch.diff(json_obj5, json_obj6);
            var reverseDelta_pkl = jsondiffpatch.reverse(delta_pkl);

            // beautiful html diff
            document.getElementById('visual').innerHTML = jsondiffpatch.formatters.html.format(reverseDelta, json_obj);
			document.getElementById('visual_gresik').innerHTML = jsondiffpatch.formatters.html.format(reverseDelta_gresik, json_obj3);

			document.getElementById('visual_pkl').innerHTML = jsondiffpatch.formatters.html.format(reverseDelta_pkl, json_obj5);



			
			jsondiffpatch.formatters.html.hideUnchanged();


            // self-explained json
            //document.getElementById('annotated').innerHTML = jsondiffpatch.formatters.annotated.format(reverseDelta, json_obj);
        </script>
    </body>
</html>