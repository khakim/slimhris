<?php


require 'Slim/Slim.php';
require '../../activerecord/ActiveRecord.php'; 

// UBAH KONEKSI DATABASE ADA STRING SEPERTI DIBAWAH INI
// 'development' => 'mysql://software:software@192.168.15.19/hris'
ActiveRecord\Config::initialize(function($cfg) {
    $cfg->set_model_directory('../../models');
    $cfg->set_connections(array(
        // Lokal Ane
        // 'development' => 'mysql://root@localhost/slimhris'
        
        // Devel 
        'development' => 'mysql://software:software@192.168.15.19/hris'
    ));
});

date_default_timezone_set('Asia/Jakarta');

\Slim\Slim::registerAutoloader();

use XBase\Table;


$app = new \Slim\Slim();

function dD($string){
		//$string = 'Wed, 13 Apr 2011 00:00:00 +0700';
		//$string = 'April 15, 2003';
		$pattern = '/[0-9]+:[0-9]+:[0-9][+0-9]/i';
		$replacement = '';
		$string = preg_replace($pattern, $replacement, $string);
		$pattern = '/\+[0-9][0-9][0-9][0-9]/i';
		$replacement = '';
		$string = preg_replace($pattern, $replacement, $string);
		$pattern = '/[a-zA-Z][a-zA-Z][a-zA-Z],/i';
		$replacement = '';
		$string = preg_replace($pattern, $replacement, $string);
		$pattern = '/(\d+) (\w+) (\d+)/i';
		$replacement = '$3-$2-$1';
		$string = preg_replace($pattern, $replacement, $string);
		$patterns = array();
		$patterns[0] = '/Jan/';
		$patterns[1] = '/Feb/';
		$patterns[2] = '/Mar/';
		$patterns[3] = '/Apr/';
		$patterns[4] = '/May/';
		$patterns[5] = '/Jun/';
		$patterns[6] = '/Jul/';
		$patterns[7] = '/Aug/';
		$patterns[8] = '/Sep/';
		$patterns[9] = '/Oct/';
		$patterns[10] = '/Nov/';
		$patterns[11] = '/Dec/';
		$replacements = array();
		$replacements[0] = '01';
		$replacements[1] = '02';
		$replacements[2] = '03';
		$replacements[3] = '04';
		$replacements[4] = '05';
		$replacements[5] = '06';
		$replacements[6] = '07';
		$replacements[7] = '08';
		$replacements[8] = '09';
		$replacements[9] = '10';
		$replacements[10] = '11';
		$replacements[11] = '12';
		return preg_replace($patterns, $replacements, $string);
		
}

function validateApiKey($key) {
	$data = tbl_api_reg::find('all', array('conditions' => array('api_key' => $key)));
	return count($data);
}

$authKey = function ($route) {
    $app = \Slim\Slim::getInstance();
    $routeParams = $route->getParams();
    if (validateApiKey($routeParams["key"])==0) {
      $app->halt(401);
    }
};


$app->get('/dbfkry/:key/', $authKey, function () use ($app)  {
    
    //$table = new Table(dirname(__FILE__).'/dbf/PDN/mst_kary.dbf', null, 'UTF-8');
    
    // Devel 15.19 Linux
    $table = new Table('/dbf/PDN/mst_kary.dbf', null, 'UTF-8');
    $dbf_jabatan = new Table('/dbf/PDN/jabatan.dbf', null, 'UTF-8');
    //$dbf_komponen_gaji = new Table('/dbf/PDN/jabatan.dbf', null, 'UTF-8');
    
    //Lokal Ane
    //$table = new Table(dirname(__FILE__).'/dbf/PDN/mst_kary.dbf', null, 'UTF-8');
    //$dbf_jabatan = new Table(dirname(__FILE__).'/dbf/PDN/jabatan.dbf', null, 'UTF-8');
    //$dbf_komponen_gaji = new Table(dirname(__FILE__).'/dbf/PDN/kary1115.dbf', null, 'UTF-8');
    
     $data_jab  = array();
     while ($record_jab = $dbf_jabatan->nextRecord()) {
     	 $data_jab[$record_jab->getChar('kode')] =	array(
			    	'kode' => $record_jab->getChar('kode'),
			    	'unit' => $record_jab->getChar('unit'),
			    	'klas' => $record_jab->getChar('klas'),
			    	'departemen' => $record_jab->getChar('dept'),
			    	'seksi' => $record_jab->getChar('seksi'),
			    	'divisi' => $record_jab->getChar('bagian'),
			    	'namajab' => $record_jab->getChar('namajab') 
			   );
 	 }
 	 
    $dbf_jabatan = json_encode(array('jabatan' => $data_jab));
	$json_jab = json_decode($dbf_jabatan);
    
    /*$data_komponen_gaji  = array();
     while ($record_komponen = $dbf_komponen_gaji->nextRecord()) {
    	   $data_komponen_gaji[$record_komponen->getChar('no_reg')] =	array(
			    	'gaji_pokok' => $record_komponen->getChar('gaji_pokok'),
			    	'kode' => $record_komponen->getChar('kode'),
			    	'namajab' => $record_komponen->getChar('namajab') 
			   );
 	 }
 	 
    $dbf_komponen_gaji = json_encode(array('komponen' => $data_komponen_gaji));
	$json_komponen = json_decode($dbf_komponen_gaji);
    */
    
    
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    
    try {
       $data  = array();
        while ($record = $table->nextRecord()) {
        
        		$cek_reg_1 = substr($record->getChar('no_reg'),0,1);
        		$cek_reg_salah = substr($record->getChar('no_reg'),1,1);
				//K1173
        		if($cek_reg_1=='K' || $cek_reg_1=='M' && is_numeric($cek_reg_salah) ){
					$fix_reg = 'D'.$record->getChar('no_reg');
					$noreg_dbf = $fix_reg;
				}else{
					$noreg_dbf = $record->getChar('no_reg');
				}
				
				$kode_jab_dbf 		= strtoupper($record->getChar('kode'));
				$klas_dbf = $record->getChar('klas');
				
				if($kode_jab_dbf!=''){
					$ar_jab = explode('.',$kode_jab_dbf);
					$kode_digit_jab = $ar_jab[1];
				    if($kode_digit_jab>9){
						$jenis_jab = "OPERATOR";
					}else{
						
						if($kode_digit_jab>=8){
							$jenis_jab = "JUNIOR SUPERVISOR";	
						}elseif($kode_digit_jab==7){
							$jenis_jab = "SUPERVISOR";	
						}elseif($kode_digit_jab==6){
							$jenis_jab = "SENIOR SUPERVISOR";	
						}elseif($kode_digit_jab==5){
							$jenis_jab = "JUNIOR MANAGER";	
						}elseif($kode_digit_jab==4){
							$jenis_jab = "SENIOR MANAGER";	
						}else{
							$jenis_jab = NULL;	
						}
						
					}	
				}else{
						$jenis_jab = NULL;
				}
				
				
				if(isset($json_jab->jabatan->$kode_jab_dbf)){
					
						 if($json_jab->jabatan->$kode_jab_dbf && $json_jab->jabatan->$kode_jab_dbf->klas==$klas_dbf){
						 	$unit_dbf 			= $json_jab->jabatan->$kode_jab_dbf->unit;
							$departemen_dbf 	= $json_jab->jabatan->$kode_jab_dbf->departemen;
							$seksi_dbf 			= $json_jab->jabatan->$kode_jab_dbf->seksi;
							$divisi_dbf 		= $json_jab->jabatan->$kode_jab_dbf->divisi;
						 }
						 
					}else{
						$nama_jabatan_dbf	= strtoupper($record->getChar('namajab'));
						$unit_dbf 			= "";
						$departemen_dbf 	= "";
						$seksi_dbf 			= "";
						$divisi_dbf 		= "";
					}
					
					
				/*if(isset($json_komponen->komponen->$noreg_dbf)){
						$gaji_pokok 		= $json_komponen->komponen->$noreg_dbf->gaji_pokok;
					}else{
						$gaji_pokok			= "0";
					}*/
						
        $data[$noreg_dbf] =	array(
		    	'tgl_masuk' => str_replace(' ','',dD(trim($record->tgl_masuk))),
		    	'tgl_keluar' => str_replace(' ','',dD(trim($record->tgl_keluar))),
		    	'nama' => strtoupper($record->getChar('nama')),
		    	'nama_jabatan' => strtoupper($nama_jabatan_dbf),
		    	'jenis' => strtoupper($jenis_jab),
		    	//'gaji_pokok' => (($gaji_pokok==NULL || $gaji_pokok=='')? '0':round($gaji_pokok,2)),
		    	'grup' => ((strtoupper($record->getChar('grup'))==NULL || strtoupper($record->getChar('grup'))=='')? '':strtoupper($record->getChar('grup'))),
		    	'status_kry' => (($record->getChar('status')==NULL || $record->getChar('status')=='')? '':$record->getChar('status')) 
		   );
        	
		    
		}
        $response->status(200);
        $response->body(json_encode($data,JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});

$app->get('/mykry/:key/', $authKey, function () use ($app)  {
	
    $data = makary::all(array(
    'select'=> 'a.*, b.tgl_masuk, b.tgl_keluar, b.grup', 
    'from' => 'karyawan as a',
    'conditions' => array('a.lokasi=?','PANDAAN'),
    'joins' => 'LEFT JOIN detail_karyawan b ON a.no_reg = b.no_reg')
    );
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    $datax = array();
    
    try {
       foreach ($data as $ax) :
       	 		$cek_reg_1 = substr($ax->no_reg,0,1);
        		$cek_reg_salah = substr($ax->no_reg,1,1);
				//K1173
        		if($cek_reg_1=='K' || $cek_reg_1=='M' && is_numeric($cek_reg_salah) ){
					$fix_reg = 'D'.$ax->no_reg;
					$noreg_dbf = $fix_reg;
				}else{
					$noreg_dbf = $ax->no_reg;
				}
				
        $datax[$noreg_dbf] =	array(
		    	'tgl_masuk' => $ax->tgl_masuk,
		    	'tgl_keluar' => (($ax->tgl_keluar==NULL || $ax->tgl_keluar=='')? '':$ax->tgl_keluar),
		    	'nama' => strtoupper($ax->nama),
		    	'nama_jabatan' => strtoupper($ax->posisi),
		    	'jenis' => strtoupper($ax->jabatan),
		    	//'gaji_pokok' => round($ax->gaji_pokok,2),
		    	'grup' => (($ax->grup==NULL || $ax->grup=='')? '':$ax->grup),
		    	'status_kry' => ((strtoupper($ax->status_kary)==NULL || strtoupper($ax->status_kary)=='')? '':strtoupper($ax->status_kary)),
		   );
	   endforeach;
        
        $response->status(200);
        $response->body(json_encode($datax,JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});

$app->get('/dbfkry/grs/:key/', $authKey, function () use ($app)  {
    
    //$table = new Table(dirname(__FILE__).'/dbf/GRS/mst_kary.dbf', null, 'UTF-8');
    //$dbf_jabatan = new Table(dirname(__FILE__).'/dbf/GRS/jabatan.dbf', null, 'UTF-8');
    
    // Devel 15.19 Linux
    $table = new Table('/dbf/ATM/mst_kary.dbf', null, 'UTF-8');
    $dbf_jabatan = new Table('/dbf/ATM/jabatan.dbf', null, 'UTF-8');
    
     $data_jab  = array();
     while ($record_jab = $dbf_jabatan->nextRecord()) {
    	   $data_jab[$record_jab->getChar('kode')] =	array(
			    	'kode' => $record_jab->getChar('kode'),
			    	'unit' => $record_jab->getChar('unit'),
			    	'departemen' => $record_jab->getChar('departemen'),
			    	'seksi' => $record_jab->getChar('seksi'),
			    	'namajab' => $record_jab->getChar('namajab') 
			   );
 	 }
    $dbf_jabatan = json_encode(array('jabatan' => $data_jab));
	$json_jab = json_decode($dbf_jabatan);
    
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    
    try {
       $data  = array();
        while ($record = $table->nextRecord()) {
        
        		$noreg_dbf 			= strtoupper($record->getChar('no_reg'));
        		$kode_jab_dbf 		= strtoupper($record->getChar('kode'));
        		$klas_dbf 			= strtoupper($record->getChar('klas'));
				
				if($kode_jab_dbf!=''){
					$ar_jab = explode('.',$kode_jab_dbf);
					$kode_digit_jab = $ar_jab[1];
				    if($kode_digit_jab>9){
						$jenis_jab = "OPERATOR";
					}else{
						
						if($kode_digit_jab>=8){
							$jenis_jab = "JUNIOR SUPERVISOR";	
						}elseif($kode_digit_jab==7){
							$jenis_jab = "SUPERVISOR";	
						}elseif($kode_digit_jab==6){
							$jenis_jab = "SENIOR SUPERVISOR";	
						}elseif($kode_digit_jab==5){
							$jenis_jab = "JUNIOR MANAGER";	
						}elseif($kode_digit_jab==4){
							$jenis_jab = "SENIOR MANAGER";	
						}else{
							$jenis_jab = NULL;	
						}
						
					}	
				}else{
						$jenis_jab = NULL;
				}
					
					
					if(isset($json_jab->jabatan->$kode_jab_dbf)){
							$nama_jabatan_dbf   = $json_jab->jabatan->$kode_jab_dbf->namajab;
						 	$unit_dbf 			= $json_jab->jabatan->$kode_jab_dbf->unit;
							$departemen_dbf 	= $json_jab->jabatan->$kode_jab_dbf->departemen;
							$seksi_dbf 			= $json_jab->jabatan->$kode_jab_dbf->seksi;
							$divisi_dbf 		= "";
					}else{
						$nama_jabatan_dbf	= "";
						$unit_dbf 			= "";
						$departemen_dbf 	= "";
						$seksi_dbf 			= "";
						$divisi_dbf 		= "";
					}
					
        $data[$noreg_dbf] =	array(
		    	'tgl_masuk' => str_replace(' ','',dD(trim($record->tgl_masuk))),
		    	'tgl_keluar' => str_replace(' ','',dD(trim($record->tgl_keluar))),
		    	'nama' => strtoupper($record->getChar('nama')),
		    	'nama_jabatan' => strtoupper($nama_jabatan_dbf),
		    	'jenis' => strtoupper($jenis_jab),
		    	'grup' => ((strtoupper($record->getChar('grup'))==NULL || strtoupper($record->getChar('grup'))=='')? '':strtoupper($record->getChar('grup'))),
		    	'status_kry' => (($record->getChar('status')==NULL || $record->getChar('status')=='')? '':$record->getChar('status')) 
		   );
        	
		    
		}
        $response->status(200);
        $response->body(json_encode($data,JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});

$app->get('/mykry/grs/:key/', $authKey, function () use ($app)  {
	
    $data = makary::all(array(
    'select'=> 'a.*, b.tgl_masuk, b.tgl_keluar, b.grup', 
    'from' => 'karyawan as a',
    'conditions' => array('a.lokasi=?','GRESIK'),
    'joins' => 'LEFT JOIN detail_karyawan b ON a.no_reg = b.no_reg')
    );
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    $datax = array();
    
    try {
       foreach ($data as $ax) :
       		
		$noreg_dbf = $ax->no_reg;
			
        $datax[$noreg_dbf] =	array(
		    	'tgl_masuk' => $ax->tgl_masuk,
		    	'tgl_keluar' => (($ax->tgl_keluar==NULL || $ax->tgl_keluar=='')? '':$ax->tgl_keluar),
		    	'nama' => strtoupper($ax->nama),
		    	'nama_jabatan' => strtoupper($ax->posisi),
		    	'jenis' => strtoupper($ax->jabatan),
		    	'grup' => (($ax->grup==NULL || $ax->grup=='')? '':$ax->grup),
		    	'status_kry' => ((strtoupper($ax->status_kary)==NULL || strtoupper($ax->status_kary)=='')? '':strtoupper($ax->status_kary)),
		   );
	   endforeach;
        
        $response->status(200);
        $response->body(json_encode($datax,JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});


$app->get('/dbfkry/pkl/:key/', $authKey, function () use ($app)  {
    
    //$table = new Table(dirname(__FILE__).'/dbf/PKL/mst_kary.dbf', null, 'UTF-8');
    
    // Devel 15.19 Linux
     $table = new Table('/dbf/PKL/mst_kary.dbf', null, 'UTF-8');
    //$dbf_jabatan = new Table('/dbf/PDN/jabatan.dbf', null, 'UTF-8');
    
    
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    
    try {
       $data  = array();
        while ($record = $table->nextRecord()) {
        
				$noreg_dbf = $record->getChar('no_reg');
				$kode_jab_dbf 		= strtoupper($record->getChar('kode'));
				
				if($kode_jab_dbf!=''){
					$ar_jab = explode('.',$kode_jab_dbf);
					$kode_digit_jab = $ar_jab[1];
				    if($kode_digit_jab>9){
						$jenis_jab = "OPERATOR";
					}else{
						
						if($kode_digit_jab>=8){
							$jenis_jab = "JUNIOR SUPERVISOR";	
						}elseif($kode_digit_jab==7){
							$jenis_jab = "SUPERVISOR";	
						}elseif($kode_digit_jab==6){
							$jenis_jab = "SENIOR SUPERVISOR";	
						}elseif($kode_digit_jab==5){
							$jenis_jab = "JUNIOR MANAGER";	
						}elseif($kode_digit_jab==4){
							$jenis_jab = "SENIOR MANAGER";	
						}else{
							$jenis_jab = NULL;	
						}
						
					}	
				}else{
						$jenis_jab = NULL;
				}
					
        $data[$noreg_dbf] =	array(
		    	'tgl_masuk' => str_replace(' ','',dD(trim($record->tgl_masuk))),
		    	'tgl_keluar' => '',
		    	'nama' => strtoupper($record->getChar('nama')),
		    	'nama_jabatan' => strtoupper($record->getChar('namajab')),
		    	'jenis' => strtoupper($jenis_jab),
		    	'grup' => ((strtoupper($record->getChar('grup'))==NULL || strtoupper($record->getChar('grup'))=='')? '':strtoupper($record->getChar('grup'))),
		    	'status_kry' => (($record->getChar('status')==NULL || $record->getChar('status')=='')? '':$record->getChar('status')) 
		   );
        	
		    
		}
        $response->status(200);
        $response->body(json_encode($data,JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});

$app->get('/mykry/pkl/:key/', $authKey, function () use ($app)  {
	
    $data = makary::all(array(
    'select'=> 'a.*, b.tgl_masuk, b.tgl_keluar, b.grup', 
    'from' => 'karyawan as a',
    'conditions' => array('a.lokasi=?','PEKALONGAN'),
    'joins' => 'LEFT JOIN detail_karyawan b ON a.no_reg = b.no_reg')
    );
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    $datax = array();
    
    try {
       foreach ($data as $ax) :
       			
				$noreg_dbf = $ax->no_reg;
        $datax[$noreg_dbf] =	array(
		    	'tgl_masuk' => $ax->tgl_masuk,
		    	'tgl_keluar' => (($ax->tgl_keluar==NULL || $ax->tgl_keluar=='')? '':$ax->tgl_keluar),
		    	'nama' => strtoupper($ax->nama),
		    	'nama_jabatan' => strtoupper($ax->posisi),
		    	'jenis' => strtoupper($ax->jabatan),
		    	'grup' => (($ax->grup==NULL || $ax->grup=='')? '':$ax->grup),
		    	'status_kry' => ((strtoupper($ax->status_kary)==NULL || strtoupper($ax->status_kary)=='')? '':strtoupper($ax->status_kary)),
		   );
	   endforeach;
        
        $response->status(200);
        $response->body(json_encode($datax,JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});





// SERVICE SYNC KARYAWAN PANDAAN
// YANG DIUBAH ADALAH LOKASI DBF NYA SEPERTI STRING DIBAWAH INI
// $table = new Table(dirname(__FILE__).'/mst_kary.dbf', null, 'UTF-8');
// MOHON DISESUAIKAN 
$app->get('/compare/:key/', $authKey, function () use ($app)  {
	$data  = array();
	$data_my  = array();
	$datax  = array();
	$data_hasil  = array();
	
	// Lokal Ane	
    //$table = new Table(dirname(__FILE__).'/dbf/pdn/mst_kary.dbf', null, 'UTF-8');
    //$dbf_jabatan = new Table(dirname(__FILE__).'/dbf/pdn/jabatan.dbf', null, 'UTF-8');
    //$dbf_komponen_gaji = new Table(dirname(__FILE__).'/dbf/PDN/kary1115.dbf', null, 'UTF-8');
    
    // Devel 15.19 Linux
    $table = new Table('/dbf/PDN/mst_kary.dbf', null, 'UTF-8');
    $dbf_jabatan = new Table('/dbf/PDN/jabatan.dbf', null, 'UTF-8');
    //$dbf_komponen_gaji = new Table('/dbf/PDN/kary1115.dbf', null, 'UTF-8');
    
 	 $data_jab  = array();
     while ($record_jab = $dbf_jabatan->nextRecord()) {
    	   $data_jab[$record_jab->getChar('kode')] =	array(
			    	'kode' => $record_jab->getChar('kode'),
			    	'klas' => (($record_jab->getChar('klas')==NULL || $record_jab->getChar('klas')=='')? '':$record_jab->getChar('klas')),
			    	'unit' => $record_jab->getChar('unit'),
			    	'departemen' => $record_jab->getChar('dept'),
			    	'seksi' => $record_jab->getChar('seksi'),
			    	'divisi' => $record_jab->getChar('bagian'),
			    	'namajab' => $record_jab->getChar('namajab') 
			   );
 	 }
 	 /*echo "<pre>";
	print_r(json_encode($data_jab,JSON_PRETTY_PRINT));
	echo "</pre>";*/
    $dbf_jabatan = json_encode(array('jabatan' => $data_jab));
	$json_jab = json_decode($dbf_jabatan);
	
	/*$data_komponen_gaji  = array();
     while ($record_komponen = $dbf_komponen_gaji->nextRecord()) {
    	   $data_komponen_gaji[$record_komponen->getChar('no_reg')] =	array(
			    	'gaji_pokok' => $record_komponen->getChar('gaji_pokok'),
			    	'kode' => $record_komponen->getChar('kode'),
			    	'namajab' => $record_komponen->getChar('namajab') 
			   );
 	 }
 	 
    $dbf_komponen_gaji = json_encode(array('komponen' => $data_komponen_gaji));
	$json_komponen = json_decode($dbf_komponen_gaji);
    */
    
	
    $data_my = makary::all(array(
    'select'=> 'a.*, b.tgl_masuk, b.tgl_keluar, b.grup', 
    'from' => 'karyawan as a',
    'conditions' => array('a.lokasi=?','PANDAAN'),
    'joins' => 'LEFT JOIN detail_karyawan b ON a.no_reg = b.no_reg')
    );
    
     
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
   
   
    try {
    
         foreach ($data_my as $ax) :
	    		
	    	$datax[$ax->no_reg] =	array(
		    	'no_reg' => $ax->no_reg,
		    	'tgl_masuk' => $ax->tgl_masuk,
		    	'nama' => strtoupper($ax->nama),
		    	'nama_jabatan' => strtoupper($ax->posisi),
		    	'unit' => strtoupper($ax->unit),
		    	'seksi' => strtoupper($ax->seksi),
		    	'dept' => strtoupper($ax->dept),
		    	'divisi' => strtoupper($ax->divisi),
		    	'level' => strtoupper($ax->skill),
		    	'grup' => strtoupper($ax->grup),
		    	'lokasi' => strtoupper($ax->lokasi),
		    	'jenis' => strtoupper($ax->jabatan),
		    	//'gaji_pokok' => strtoupper($ax->gaji_pokok),
		    	'tgl_keluar' => $ax->tgl_keluar,
		    	'status_kry' => strtoupper($ax->status_kary)
		   );
	   endforeach;
	   
	   $dbf_mysql = json_encode(array('karyawan' => $datax));
	   $json = json_decode($dbf_mysql);
	   $list = "";
	   $tgl_keluar_my = "";
	   $tgl_keluar_hasil = "";
	   $level_hasil = "";
	   $jenis_my = "";
	    while ($record = $table->nextRecord()) {
        	
        		$noreg_dbf 			= $record->getChar('no_reg');
        		$reg_dbf_asli 		= $record->getChar('no_reg');	
        		
        		$cek_reg_1 = substr($record->getChar('no_reg'),0,1);
        		$cek_reg_salah = substr($record->getChar('no_reg'),1,1);
				if($cek_reg_1=='K' || $cek_reg_1=='M' && is_numeric($cek_reg_salah) ){
					$fix_reg = 'D'.$record->getChar('no_reg');
					
					if(isset($json->karyawan->$reg_dbf_asli)){
						if($fix_reg!=$json->karyawan->$reg_dbf_asli->no_reg){
						
							makary::update_all(array(
							  'set' => array(
							    'no_reg' => $fix_reg
							  ),
							  'conditions' => array(
							    'no_reg' => $record->getChar('no_reg')
							  )
							));
							
							dtkary::update_all(array(
							  'set' => array(
							    'no_reg_dbf' => $reg_dbf_asli
							  ),
							  'conditions' => array(
							    'no_reg' => $fix_reg
							  )
							));
						}
					}
					
					$noreg_dbf = $fix_reg;
					
				}else{
					$noreg_dbf = $record->getChar('no_reg');
				}
				
        		$tgl_masuk_dbf 		= str_replace(' ','',dD(trim($record->tgl_masuk)));
				$nama_dbf 			= strtoupper($record->getChar('nama'));
				$nama_jabatan_dbf 	= strtoupper($record->getChar('namajab'));
				$grup_dbf 			= strtoupper($record->getChar('grup'));
				$status_kry_dbf 	= strtoupper($record->getChar('status'));
				$jns_kel_dbf 		= strtoupper($record->getChar('l_p'));
				$tgl_keluar_dbf 	= strtoupper($record->getChar('tgl_keluar'));
				$kode_jab_dbf 		= strtoupper($record->getChar('kode'));
				$klas_dbf 			= strtoupper($record->getChar('klas'));
				
				if(isset($json_jab->jabatan->$kode_jab_dbf)){
					
					 if($json_jab->jabatan->$kode_jab_dbf && $json_jab->jabatan->$kode_jab_dbf->klas==$klas_dbf){
					 	$unit_dbf 			= $json_jab->jabatan->$kode_jab_dbf->unit;
						$departemen_dbf 	= $json_jab->jabatan->$kode_jab_dbf->departemen;
						$seksi_dbf 			= $json_jab->jabatan->$kode_jab_dbf->seksi;
						$divisi_dbf 		= $json_jab->jabatan->$kode_jab_dbf->divisi;
					 }
					 
					
				}else{
					$unit_dbf 			= "";
					$departemen_dbf 	= "";
					$seksi_dbf 			= "";
					$divisi_dbf 		= "";
				}
				
				
				// A = highskill, B = skill, C=semi skill, D=unskill
				// UNSKILL,SKILL,HIGH_SKILL,SEMI_SKILL;
				if($klas_dbf=='A'){ $level_dbf = 'HIGH_SKILL'; }
				elseif($klas_dbf=='B'){ $level_dbf = 'SKILL'; }
				elseif($klas_dbf=='C'){ $level_dbf = 'SEMI_SKILL'; }
				elseif($klas_dbf=='D'){ $level_dbf = 'UNSKILL'; }
				else {$level_dbf = '';}
				
				
				if($kode_jab_dbf!=''){
					$ar_jab = explode('.',$kode_jab_dbf);
					$kode_digit_jab = $ar_jab[1];
				    if($kode_digit_jab>9){
						$jenis_jab = "OPERATOR";
					}else{
						$level_dbf = 'HIGH_SKILL';
						if($kode_digit_jab>=8){
							$jenis_jab = "JUNIOR SUPERVISOR";	
						}elseif($kode_digit_jab==7){
							$jenis_jab = "SUPERVISOR";	
						}elseif($kode_digit_jab==6){
							$jenis_jab = "SENIOR SUPERVISOR";	
						}elseif($kode_digit_jab==5){
							$jenis_jab = "JUNIOR MANAGER";	
						}elseif($kode_digit_jab==4){
							$jenis_jab = "SENIOR MANAGER";	
						}else{
							$jenis_jab = '';	
						}
						
					}	
				}else{
						$jenis_jab = '';
				}
				
				/*if(isset($json_komponen->komponen->$noreg_dbf)){
					$gaji_pokok 		= $json_komponen->komponen->$noreg_dbf->gaji_pokok;
				}else{
					$gaji_pokok			= "0";
				}*/
				
				if($jns_kel_dbf=='L'){ $jns_kel = '1'; }else{ $jns_kel = '2';}
					
	      		if(isset($json->karyawan->$reg_dbf_asli)) {
        				
        				/*if($json->karyawan->$reg_dbf_asli->gaji_pokok != $gaji_pokok)
					    {
					        $gapok_hasil = "Jenis Tidak Sama.";
					        $gapok_my 	 = $json->karyawan->$reg_dbf_asli->gaji_pokok;
					    }else{
							$gapok_hasil = "SAMA";
							$gapok_my 	 = $json->karyawan->$reg_dbf_asli->gaji_pokok;
						}*/
						
        				
        				if($json->karyawan->$reg_dbf_asli->jenis != $jenis_jab)
					    {
					        $jenis_hasil = "Jenis Tidak Sama.";
					        $jenis_my = $json->karyawan->$reg_dbf_asli->jenis;
					    }else{
							$jenis_hasil = "SAMA";
							$jenis_my 	 = $json->karyawan->$reg_dbf_asli->jenis;
						}
						
						
        				if($json->karyawan->$reg_dbf_asli->tgl_masuk != $tgl_masuk_dbf)
					    {
					        $tgl_msk_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_masuk_my = $json->karyawan->$reg_dbf_asli->tgl_masuk;
					    }else{
							$tgl_msk_hasil = "SAMA";
							$tgl_masuk_my = $json->karyawan->$reg_dbf_asli->tgl_masuk;
						}
						
						if($json->karyawan->$reg_dbf_asli->level != $level_dbf)
					    {
					        $level_hasil = "Level Tidak Sama.";
					        $level_my = $json->karyawan->$reg_dbf_asli->level;
					    }else{
							$level_hasil = "SAMA";
							$level_my = $json->karyawan->$reg_dbf_asli->level;
						}
						
					    if($json->karyawan->$reg_dbf_asli->tgl_keluar != $tgl_keluar_dbf)
					    {
					        $tgl_keluar_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_keluar_my = $json->karyawan->$reg_dbf_asli->tgl_keluar;
					    }else{
							$tgl_keluar_hasil = "SAMA";
							$tgl_keluar_my = $json->karyawan->$reg_dbf_asli->tgl_keluar;
						}
					    
					    if($json->karyawan->$reg_dbf_asli->nama != $nama_dbf){
							$nama_hasil    = "Nama Tidak Sama.";
							$nama_my    = $json->karyawan->$reg_dbf_asli->nama;
							
						    
						}else{
							$nama_hasil = "SAMA";
							$nama_my =  $json->karyawan->$reg_dbf_asli->nama;
						}
						
						if($json->karyawan->$reg_dbf_asli->nama_jabatan != $nama_jabatan_dbf ){
							$nama_jabatan_hasil = "Nama Jabatan Tidak Sama.";
							$nama_jabatan_my = $json->karyawan->$reg_dbf_asli->nama_jabatan;
							
						}else{
							$nama_jabatan_hasil = "SAMA";
							$nama_jabatan_my = $json->karyawan->$reg_dbf_asli->nama_jabatan;
						}
						
						if($json->karyawan->$reg_dbf_asli->grup != $grup_dbf ){
							$grup_hasil   = "Grup Tidak Sama.";
							$grup_my  	  = $json->karyawan->$reg_dbf_asli->grup;
							
						}else{
							$grup_hasil = "SAMA";
							$grup_my = $json->karyawan->$reg_dbf_asli->grup;
						}
						
						if($json->karyawan->$reg_dbf_asli->status_kry != $status_kry_dbf ){
							$status_kry_hasil = "Status Kry Tidak Sama.";
							$status_kry_my	  = $json->karyawan->$reg_dbf_asli->status_kry;
							
						}else{
							
							$status_kry_hasil = "SAMA";
							$status_kry_my = $json->karyawan->$reg_dbf_asli->status_kry;
						}
						
						$status_data_hasil =  'Karyawan Lama';
						
				}
				else{
						$jenis_hasil = $jenis_jab;
						//$gapok_hasil = $gaji_pokok;
						$tgl_msk_hasil = $tgl_masuk_dbf;
						$nama_hasil = $nama_dbf;
						$nama_jabatan_hasil = $nama_jabatan_dbf;
						$grup_hasil = $grup_dbf;
						$status_kry_hasil = $status_kry_dbf;
						$status_data_hasil =  'Karyawan Baru';
						$tgl_masuk_my =  $tgl_masuk_dbf;
						$nama_my =  $nama_dbf;
						$nama_jabatan_my =  $nama_jabatan_dbf;
						$grup_my =  $grup_dbf;
						$level_my =  $level_dbf;
						$status_kry_my =  $status_kry_dbf;
						
						
				}
				
				
				
				
				if(isset($json->karyawan->$noreg_dbf)) {
					
						/*if($json->karyawan->$noreg_dbf->gaji_pokok != $gaji_pokok)
					    {
					        $gapok_hasil = "Gapok Tidak Sama.";
					        $gapok_my 	 = $json->karyawan->$noreg_dbf->gaji_pokok;
					    }else{
							$gapok_hasil = "SAMA";
							$gapok_my 	 = $json->karyawan->$noreg_dbf->gaji_pokok;
						}*/
        			
        				if($json->karyawan->$noreg_dbf->jenis != $jenis_jab)
					    {
					        $jenis_hasil = "Jenis Tidak Sama.";
					        $jenis_my = $json->karyawan->$noreg_dbf->jenis;
					    }else{
							$jenis_hasil = "SAMA";
							$jenis_my 	 = $json->karyawan->$noreg_dbf->jenis;
						}
						
						
        				if($json->karyawan->$noreg_dbf->tgl_masuk != $tgl_masuk_dbf)
					    {
					        $tgl_msk_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_masuk_my = $json->karyawan->$noreg_dbf->tgl_masuk;
					    }else{
							$tgl_msk_hasil = "SAMA";
							$tgl_masuk_my = $json->karyawan->$noreg_dbf->tgl_masuk;
						}
						
						if($json->karyawan->$noreg_dbf->level != $level_dbf)
					    {
					        $level_hasil = "Level Tidak Sama.";
					        $level_my = $json->karyawan->$noreg_dbf->level;
					    }else{
							$level_hasil = "SAMA";
							$level_my = $json->karyawan->$noreg_dbf->level;
						}
						
					    if($json->karyawan->$noreg_dbf->tgl_keluar != $tgl_keluar_dbf)
					    {
					        $tgl_keluar_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_keluar_my = $json->karyawan->$noreg_dbf->tgl_keluar;
					    }else{
							$tgl_keluar_hasil = "SAMA";
							$tgl_keluar_my = $json->karyawan->$noreg_dbf->tgl_keluar;
						}
					    
					    if($json->karyawan->$noreg_dbf->nama != $nama_dbf){
							$nama_hasil    = "Nama Tidak Sama.";
							$nama_my    = $json->karyawan->$noreg_dbf->nama;
							
						    
						}else{
							$nama_hasil = "SAMA";
							$nama_my =  $json->karyawan->$noreg_dbf->nama;
						}
						
						if($json->karyawan->$noreg_dbf->nama_jabatan != $nama_jabatan_dbf ){
							$nama_jabatan_hasil = "Nama Jabatan Tidak Sama.";
							$nama_jabatan_my = $json->karyawan->$noreg_dbf->nama_jabatan;
							
						}else{
							$nama_jabatan_hasil = "SAMA";
							$nama_jabatan_my = $json->karyawan->$noreg_dbf->nama_jabatan;
						}
						
						if($json->karyawan->$noreg_dbf->grup != $grup_dbf ){
							$grup_hasil   = "Grup Tidak Sama.";
							$grup_my  	  = $json->karyawan->$noreg_dbf->grup;
							
						}else{
							$grup_hasil = "SAMA";
							$grup_my = $json->karyawan->$noreg_dbf->grup;
						}
						
						if($json->karyawan->$noreg_dbf->status_kry != $status_kry_dbf ){
							$status_kry_hasil = "Status Kry Tidak Sama.";
							$status_kry_my	  = $json->karyawan->$noreg_dbf->status_kry;
							
						}else{
							
							$status_kry_hasil = "SAMA";
							$status_kry_my = $json->karyawan->$noreg_dbf->status_kry;
						}
						
						$status_data_hasil =  'Karyawan Lama';
						
				}
				
				
				
				
				$data_hasil[] = array(
        				  'no_reg' => $noreg_dbf,
        				  'no_reg_dbf' => $reg_dbf_asli,
        				  'cek_tgl_masuk' => $tgl_msk_hasil,
        				  'tgl_masuk_my' => $tgl_masuk_my,
        				  'tgl_masuk_dbf' => $tgl_masuk_dbf,
        				  'cek_nama' => $nama_hasil,
        				  'nama_my' => $nama_my,
        				  'nama_dbf' => $nama_dbf,
        				  'cek_nama_jabatan' => $nama_jabatan_hasil,
        				  'nama_jabatan_my' => $nama_jabatan_my,
        				  'nama_jabatan_dbf' => $nama_jabatan_dbf,
        				  'cek_grup' => $grup_hasil,
        				  'grup_my' => $grup_my,
        				  'grup_dbf' => $grup_dbf,
        				  'cek_status_kry' => $status_kry_hasil,
        				  'status_kry_my' => $status_kry_my,
        				  'status_kry_dbf' => $status_kry_dbf,
        				  'status_kry' => $status_kry_dbf,
        				  'jenis_kel' => $jns_kel,
        				  'tgl_keluar_dbf' => $tgl_keluar_dbf,
        				  'tgl_keluar_my' => $tgl_keluar_my,
        				  'cek_tgl_keluar' => $tgl_keluar_hasil,
        				  'level_dbf' => $level_dbf,
        				  'level_my' => $level_my,
        				  'cek_level' => $level_hasil,
        				  'jenis_my' => $jenis_my,
        				  'jenis_dbf' => $jenis_jab,
        				  'cek_jenis' => $jenis_hasil,
        				  'unit_dbf' => $unit_dbf,
        				  'departemen_dbf' => $departemen_dbf,
        				  'seksi_dbf' => $seksi_dbf,
        				  'divisi_dbf' => $divisi_dbf,
        				 // 'gaji_pokok' => $gaji_pokok,
        				  //'cek_gaji_pokok' => $gapok_hasil,
        				  'status_data' => $status_data_hasil
        		);
				
				    
				    
				
       }
		 
        
        
	    foreach ($data_hasil as $ayx) :
		
	      if($ayx['status_data']=='Karyawan Lama'){
	      	
	      		if($ayx['cek_tgl_masuk']!='SAMA'){
	      			$cek_reg = dtkary::find(
	      			array(
	      			'select'=> 'no_reg', 
	      			'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
	      				$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->tgl_masuk = $ayx['tgl_masuk_dbf'];
					    $update->save();
					}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    $create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
	      			
	      				
					   
					
				}
				
				if($ayx['cek_tgl_keluar']!='SAMA'){
					$cek_reg = dtkary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
						$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->tgl_keluar = $ayx['tgl_keluar_dbf'];
					    $update->save();	
					}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}			
				}
				
				if($ayx['cek_nama']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->nama = $ayx['nama_dbf'];
				    $update->save();	
				}
				
				if($ayx['cek_jenis']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->jabatan = $ayx['jenis_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_nama_jabatan']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->posisi = $ayx['nama_jabatan_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_grup']!='SAMA'){
					$cek_reg = dtkary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
		      			$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->grup = $ayx['grup_dbf'];
					    $update->save();
	      			}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
					
				}
				
				
				if($ayx['cek_level']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->skill = $ayx['level_dbf'];
				    $update->save();
				}
				
				/*if($ayx['cek_gaji_pokok']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->gaji_pokok = $ayx['gaji_pokok'];
				    $update->save();
				}*/
				
				if($ayx['cek_status_kry']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->status_kary = $ayx['status_kry'];
				    $update->save();
				}
				
		  }
		  
		  if($ayx['status_data']=='Karyawan Baru'){
		  		
		  			$cek_reg = makary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg==0){
	      				$create_karyawan = new makary();
					    $create_karyawan->nama 					= $ayx['nama_dbf'];
					    $create_karyawan->no_reg 				= $ayx['no_reg'];
					    $create_karyawan->posisi		  		= $ayx['nama_jabatan_dbf'];
					    $create_karyawan->status_kary 			= $ayx['status_kry'];
					    $create_karyawan->skill 				= $ayx['level_dbf'];
					    $create_karyawan->jabatan 				= $ayx['jenis_dbf'];
					    $create_karyawan->unit 					= $ayx['unit_dbf'];
					    $create_karyawan->seksi 				= $ayx['seksi_dbf'];
					    $create_karyawan->dept 					= $ayx['departemen_dbf'];
					    $create_karyawan->divisi 				= $ayx['divisi_dbf'];
					    //$create_karyawan->gaji_pokok 			= $ayx['gaji_pokok'];
					    $create_karyawan->lokasi 				= 'PANDAAN';
					    $create_karyawan->save();
					    
					   	$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
		  		
	      	 
			    
			   
		  	
		  }
		  
	    	
	   endforeach;
	   
        $response->status(200);
        $response->body(json_encode(array('data_karyawan' => $data_hasil),JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});


// SERVICE SYNC KARYAWAN GRESIK
// YANG DIUBAH ADALAH LOKASI DBF NYA SEPERTI STRING DIBAWAH INI
// $table = new Table(dirname(__FILE__).'/mst_kary.dbf', null, 'UTF-8');
// MOHON DISESUAIKAN 
$app->get('/compare/grs/:key/', $authKey, function () use ($app)  {
	$data  = array();
	$data_my  = array();
	$datax  = array();
	$data_hasil  = array();
	
	// Lokal Ane	
    //$table = new Table(dirname(__FILE__).'/dbf/grs/mst_kary.dbf', null, 'UTF-8');
    //$dbf_jabatan = new Table(dirname(__FILE__).'/dbf/grs/jabatan.dbf', null, 'UTF-8');
    
    // Devel 15.19 Linux
    $table = new Table('/dbf/ATM/mst_kary.dbf', null, 'UTF-8');
    $dbf_jabatan = new Table('/dbf/ATM/jabatan.dbf', null, 'UTF-8');
    
     
 	 $data_jab  = array();
     while ($record_jab = $dbf_jabatan->nextRecord()) {
    	   $data_jab[$record_jab->getChar('kode')] =	array(
			    	'kode' => $record_jab->getChar('kode'),
			    	'unit' => $record_jab->getChar('unit'),
			    	'departemen' => $record_jab->getChar('departemen'),
			    	'seksi' => $record_jab->getChar('seksi'),
			    	'namajab' => $record_jab->getChar('namajab') 
			   );
 	 }
    $dbf_jabatan = json_encode(array('jabatan' => $data_jab));
	$json_jab = json_decode($dbf_jabatan);
	
	
    
    $data_my = makary::all(array(
    'select'=> 'a.*, b.tgl_masuk, b.tgl_keluar, b.grup', 
    'from' => 'karyawan as a',
    'conditions' => array('a.lokasi=?','GRESIK'),
    'joins' => 'LEFT JOIN detail_karyawan b ON a.no_reg = b.no_reg')
    );
    
     
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    
   
    try {
    	
         foreach ($data_my as $ax) :
	    		
	    	$datax[$ax->no_reg] =	array(
		    	'no_reg' => $ax->no_reg,
		    	'tgl_masuk' => $ax->tgl_masuk,
		    	'nama' => strtoupper($ax->nama),
		    	'nama_jabatan' => strtoupper($ax->nama_jabatan),
		    	'unit' => strtoupper($ax->unit),
		    	'seksi' => strtoupper($ax->seksi),
		    	'dept' => strtoupper($ax->dept),
		    	'divisi' => strtoupper($ax->divisi),
		    	'level' => strtoupper($ax->level),
		    	'grup' => strtoupper($ax->grup),
		    	'lokasi' => strtoupper($ax->lokasi),
		    	'jenis' => strtoupper($ax->jenis),
		    	'tgl_keluar' => $ax->tgl_keluar,
		    	'status_kry' => strtoupper($ax->status_kary)
		   );
	   endforeach;
	   
	   $dbf_mysql = json_encode(array('karyawan' => $datax));
	   $json = json_decode($dbf_mysql);
	   $list = "";
	   $tgl_keluar_my = "";
	   $tgl_keluar_hasil = "";
	   $level_hasil = "";
	   $jenis_my = "";
	    while ($record = $table->nextRecord()) {
        	
        		$noreg_dbf 			= $record->getChar('no_reg');
        		$tgl_masuk_dbf 		= str_replace(' ','',dD(trim($record->tgl_masuk)));
				$nama_dbf 			= strtoupper($record->getChar('nama'));
				$grup_dbf 			= strtoupper($record->getChar('grup'));
				$status_kry_dbf 	= strtoupper($record->getChar('status'));
				$jns_kel_dbf 		= strtoupper($record->getChar('l_p'));
				$tgl_keluar_dbf 	= strtoupper($record->getChar('tgl_keluar'));
				$kode_jab_dbf 		= strtoupper($record->getChar('kode'));
				$klas_dbf 			= strtoupper($record->getChar('skill'));
				if(isset($json_jab->jabatan->$kode_jab_dbf)){
					$nama_jabatan_dbf 	= $json_jab->jabatan->$kode_jab_dbf->namajab;
					$unit_dbf 			= $json_jab->jabatan->$kode_jab_dbf->unit;
					$departemen_dbf 	= $json_jab->jabatan->$kode_jab_dbf->departemen;
					$seksi_dbf 			= $json_jab->jabatan->$kode_jab_dbf->seksi;
					$divisi_dbf 		= "";
				}else{
					$nama_jabatan_dbf	= "";
					$unit_dbf 			= "";
					$departemen_dbf 	= "";
					$seksi_dbf 			= "";
					$divisi_dbf 		= "";
				}
				
				
				// A = highskill, B = skill, C=semi skill, D=unskill
				// UNSKILL,SKILL,HIGH_SKILL,SEMI_SKILL;
				if($klas_dbf=='HS'){ $level_dbf = 'HIGH_SKILL'; }
				elseif($klas_dbf=='SK'){ $level_dbf = 'SKILL'; }
				elseif($klas_dbf=='SS'){ $level_dbf = 'SEMI_SKILL'; }
				elseif($klas_dbf=='US'){ $level_dbf = 'UNSKILL'; }
				else {$level_dbf = '';}
				
				
				
				if($kode_jab_dbf!=''){
					$ar_jab = explode('.',$kode_jab_dbf);
					$kode_digit_jab = $ar_jab[1];
				    if($kode_digit_jab>9){
						$jenis_jab = "OPERATOR";
					}else{
						
						$level_dbf = 'HIGH_SKILL';
						
						if($kode_digit_jab>=8){
							$jenis_jab = "JUNIOR SUPERVISOR";	
						}elseif($kode_digit_jab==7){
							$jenis_jab = "SUPERVISOR";	
						}elseif($kode_digit_jab==6){
							$jenis_jab = "SENIOR SUPERVISOR";	
						}elseif($kode_digit_jab==5){
							$jenis_jab = "JUNIOR MANAGER";	
						}elseif($kode_digit_jab==4){
							$jenis_jab = "SENIOR MANAGER";	
						}else{
							$jenis_jab = '';	
						}
						
					}	
				}else{
						$jenis_jab = '';
				}
				
				
				
				
				
				if($jns_kel_dbf=='L'){ $jns_kel = '1'; }else{ $jns_kel = '2';}
				
				if(isset($json->karyawan->$noreg_dbf)) {
        			
        				if($json->karyawan->$noreg_dbf->jenis != $jenis_jab)
					    {
					        $jenis_hasil = "Jenis Tidak Sama.";
					        $jenis_my = $json->karyawan->$noreg_dbf->jenis;
					    }else{
							$jenis_hasil = "SAMA";
							$jenis_my 	 = $json->karyawan->$noreg_dbf->jenis;
						}
						
						
        				if($json->karyawan->$noreg_dbf->tgl_masuk != $tgl_masuk_dbf)
					    {
					        $tgl_msk_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_masuk_my = $json->karyawan->$noreg_dbf->tgl_masuk;
					    }else{
							$tgl_msk_hasil = "SAMA";
							$tgl_masuk_my = $json->karyawan->$noreg_dbf->tgl_masuk;
						}
						
						if($json->karyawan->$noreg_dbf->level != $level_dbf)
					    {
					        $level_hasil = "Level Tidak Sama.";
					        $level_my = $json->karyawan->$noreg_dbf->level;
					    }else{
							$level_hasil = "SAMA";
							$level_my = $json->karyawan->$noreg_dbf->level;
						}
						
					    if($json->karyawan->$noreg_dbf->tgl_keluar != $tgl_keluar_dbf)
					    {
					        $tgl_keluar_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_keluar_my = $json->karyawan->$noreg_dbf->tgl_keluar;
					    }else{
							$tgl_keluar_hasil = "SAMA";
							$tgl_keluar_my = $json->karyawan->$noreg_dbf->tgl_keluar;
						}
					    
					    if($json->karyawan->$noreg_dbf->nama != $nama_dbf){
							$nama_hasil    = "Nama Tidak Sama.";
							$nama_my    = $json->karyawan->$noreg_dbf->nama;
							
						    
						}else{
							$nama_hasil = "SAMA";
							$nama_my =  $json->karyawan->$noreg_dbf->nama;
						}
						
						if($json->karyawan->$noreg_dbf->nama_jabatan != $nama_jabatan_dbf ){
							$nama_jabatan_hasil = "Nama Jabatan Tidak Sama.";
							$nama_jabatan_my = $json->karyawan->$noreg_dbf->nama_jabatan;
							
						}else{
							$nama_jabatan_hasil = "SAMA";
							$nama_jabatan_my = $json->karyawan->$noreg_dbf->nama_jabatan;
						}
						
						if($json->karyawan->$noreg_dbf->grup != $grup_dbf ){
							$grup_hasil   = "Grup Tidak Sama.";
							$grup_my  	  = $json->karyawan->$noreg_dbf->grup;
							
						}else{
							$grup_hasil = "SAMA";
							$grup_my = $json->karyawan->$noreg_dbf->grup;
						}
						
						if($json->karyawan->$noreg_dbf->status_kry != $status_kry_dbf ){
							$status_kry_hasil = "Status Kry Tidak Sama.";
							$status_kry_my	  = $json->karyawan->$noreg_dbf->status_kry;
							
						}else{
							
							$status_kry_hasil = "SAMA";
							$status_kry_my = $json->karyawan->$noreg_dbf->status_kry;
						}
						
						$status_data_hasil =  'Karyawan Lama';
						
				}else{
						$jenis_hasil = $jenis_jab;
						$tgl_msk_hasil = $tgl_masuk_dbf;
						$nama_hasil = $nama_dbf;
						$nama_jabatan_hasil = $nama_jabatan_dbf;
						$grup_hasil = $grup_dbf;
						$status_kry_hasil = $status_kry_dbf;
						$status_data_hasil =  'Karyawan Baru';
						$tgl_masuk_my =  $tgl_masuk_dbf;
						$nama_my =  $nama_dbf;
						$nama_jabatan_my =  $nama_jabatan_dbf;
						$grup_my =  $grup_dbf;
						$level_my =  $level_dbf;
						$status_kry_my =  $status_kry_dbf;
						
						
				}
				
				
				
				
				$data_hasil[] = array(
        				  'no_reg' => $noreg_dbf,
        				  'no_reg_dbf' => $noreg_dbf,
        				  'cek_tgl_masuk' => $tgl_msk_hasil,
        				  'tgl_masuk_my' => $tgl_masuk_my,
        				  'tgl_masuk_dbf' => $tgl_masuk_dbf,
        				  'cek_nama' => $nama_hasil,
        				  'nama_my' => $nama_my,
        				  'nama_dbf' => $nama_dbf,
        				  'cek_nama_jabatan' => $nama_jabatan_hasil,
        				  'nama_jabatan_my' => $nama_jabatan_my,
        				  'nama_jabatan_dbf' => $nama_jabatan_dbf,
        				  'cek_grup' => $grup_hasil,
        				  'grup_my' => $grup_my,
        				  'grup_dbf' => $grup_dbf,
        				  'cek_status_kry' => $status_kry_hasil,
        				  'status_kry_my' => $status_kry_my,
        				  'status_kry_dbf' => $status_kry_dbf,
        				  'status_kry' => $status_kry_dbf,
        				  'jenis_kel' => $jns_kel,
        				  'tgl_keluar_dbf' => $tgl_keluar_dbf,
        				  'tgl_keluar_my' => $tgl_keluar_my,
        				  'cek_tgl_keluar' => $tgl_keluar_hasil,
        				  'level_dbf' => $level_dbf,
        				  'level_my' => $level_my,
        				  'cek_level' => $level_hasil,
        				  'jenis_my' => $jenis_my,
        				  'jenis_dbf' => $jenis_jab,
        				  'cek_jenis' => $jenis_hasil,
        				  'unit_dbf' => $unit_dbf,
        				  'departemen_dbf' => $departemen_dbf,
        				  'seksi_dbf' => $seksi_dbf,
        				  'divisi_dbf' => $divisi_dbf,
        				  'status_data' => $status_data_hasil
        		);
				
				    
				    
				
       }
		 
        
        
	    foreach ($data_hasil as $ayx) :
		
	      if($ayx['status_data']=='Karyawan Lama'){
	      	
	      		if($ayx['cek_tgl_masuk']!='SAMA'){
	      			$cek_reg = dtkary::find(
	      			array(
	      			'select'=> 'no_reg', 
	      			'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
	      				$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->tgl_masuk = $ayx['tgl_masuk_dbf'];
					    $update->save();
					}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
	      			
	      				
					   
					
				}
				
				if($ayx['cek_tgl_keluar']!='SAMA'){
					$cek_reg = dtkary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
						$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->tgl_keluar = $ayx['tgl_keluar_dbf'];
					    $update->save();	
					}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}			
				}
				
				if($ayx['cek_nama']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->nama = $ayx['nama_dbf'];
				    $update->save();	
				}
				
				if($ayx['cek_jenis']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->jabatan = $ayx['jenis_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_nama_jabatan']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->posisi = $ayx['nama_jabatan_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_grup']!='SAMA'){
					$cek_reg = dtkary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
		      			$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->grup = $ayx['grup_dbf'];
					    $update->save();
	      			}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
					
				}
				
				
				if($ayx['cek_level']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->skill = $ayx['level_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_status_kry']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->status_kary = $ayx['status_kry'];
				    $update->save();
				}
				
		  }
		  
		  if($ayx['status_data']=='Karyawan Baru'){
		  		
		  			$cek_reg = makary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg==0){
	      				$create_karyawan = new makary();
					    $create_karyawan->nama 					= $ayx['nama_dbf'];
					    $create_karyawan->no_reg 				= $ayx['no_reg'];
					    $create_karyawan->posisi		  		= $ayx['nama_jabatan_dbf'];
					    $create_karyawan->status_kary 			= $ayx['status_kry'];
					    $create_karyawan->skill 				= $ayx['level_dbf'];
					    $create_karyawan->jabatan 				= $ayx['jenis_dbf'];
					    $create_karyawan->unit 					= $ayx['unit_dbf'];
					    $create_karyawan->seksi 				= $ayx['seksi_dbf'];
					    $create_karyawan->dept 					= $ayx['departemen_dbf'];
					    $create_karyawan->divisi				= $ayx['divisi_dbf'];
					    //$create_karyawan->gaji_pokok 			= '0';
					    $create_karyawan->lokasi 				= 'ATM';
					    $create_karyawan->save();
					    
					   	$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
		  		
	      	 
			    
			   
		  	
		  }
		  
	    	
	   endforeach;
	   
        $response->status(200);
        $response->body(json_encode(array('data_karyawan' => $data_hasil),JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});

// SERVICE SYNC KARYAWAN PEKALONGAN
// YANG DIUBAH ADALAH LOKASI DBF NYA SEPERTI STRING DIBAWAH INI
// $table = new Table(dirname(__FILE__).'/mst_kary.dbf', null, 'UTF-8');
// MOHON DISESUAIKAN 
$app->get('/compare/pkl/:key/', $authKey, function () use ($app)  {
	$data  = array();
	$data_my  = array();
	$datax  = array();
	$data_hasil  = array();
	
	// Lokal Ane	
    //$table = new Table(dirname(__FILE__).'/dbf/pkl/mst_kary.dbf', null, 'UTF-8');
    
     // Devel 15.19 Linux
    $table = new Table('/dbf/PKL/mst_kary.dbf', null, 'UTF-8');
    //$dbf_jabatan = new Table('/dbf/GRS/jabatan.dbf', null, 'UTF-8');
   
   
    $data_my = makary::all(array(
    'select'=> 'a.*, b.tgl_masuk, b.tgl_keluar, b.grup', 
    'from' => 'karyawan as a',
    'conditions' => array('a.lokasi=?','PEKALONGAN'),
    'joins' => 'LEFT JOIN detail_karyawan b ON a.no_reg = b.no_reg')
    );
    
     
    $response = $app->response();
    $response['Content-Type'] = 'application/json';
    $response['X-Powered-By'] = 'BTX';
    
   
    try {
    
         foreach ($data_my as $ax) :
	    		
	    	$datax[$ax->no_reg] =	array(
		    	'no_reg' => $ax->no_reg,
		    	'tgl_masuk' => $ax->tgl_masuk,
		    	'nama' => strtoupper($ax->nama),
		    	'nama_jabatan' => strtoupper($ax->nama_jabatan),
		    	'unit' => strtoupper($ax->unit),
		    	'seksi' => strtoupper($ax->seksi),
		    	'dept' => strtoupper($ax->dept),
		    	'divisi' => strtoupper($ax->divisi),
		    	'level' => strtoupper($ax->level),
		    	'grup' => strtoupper($ax->grup),
		    	'lokasi' => strtoupper($ax->lokasi),
		    	'jenis' => strtoupper($ax->jenis),
		    	'tgl_keluar' => $ax->tgl_keluar,
		    	'status_kry' => strtoupper($ax->status_kary)
		   );
	   endforeach;
	   
	   $dbf_mysql = json_encode(array('karyawan' => $datax));
	   $json = json_decode($dbf_mysql);
	   $list = "";
	   $tgl_keluar_dbf = "";
	   $tgl_keluar_my = "";
	   $tgl_keluar_hasil = "";
	   $level_hasil = "";
	   $jenis_my = "";
	    while ($record = $table->nextRecord()) {
        	
        		$noreg_dbf 			= $record->getChar('no_reg');
        		$tgl_masuk_dbf 		= str_replace(' ','',dD(trim($record->tgl_masuk)));
				$nama_dbf 			= strtoupper($record->getChar('nama'));
				$nama_jabatan_dbf 	= strtoupper($record->getChar('namajab'));
				$grup_dbf 			= strtoupper($record->getChar('grup'));
				$status_kry_dbf 	= strtoupper($record->getChar('status'));
				$jns_kel_dbf 		= strtoupper($record->getChar('l_p'));
				//$tgl_keluar_dbf 	= strtoupper($record->getChar('tgl_keluar'));
				$kode_jab_dbf 		= strtoupper($record->getChar('kode'));
				$klas_dbf 			= strtoupper($record->getChar('klas'));
				$unit_dbf 			= "";
				$departemen_dbf 	= "";
				$seksi_dbf 			= "";
				$divisi_dbf 		= "";
				
				// A = highskill, B = skill, C=semi skill, D=unskill
				// UNSKILL,SKILL,HIGH_SKILL,SEMI_SKILL;
				if($klas_dbf=='1'){ $level_dbf = 'HIGH_SKILL'; }
				elseif($klas_dbf=='2'){ $level_dbf = 'SKILL'; }
				elseif($klas_dbf=='3'){ $level_dbf = 'SEMI_SKILL'; }
				elseif($klas_dbf=='4'){ $level_dbf = 'UNSKILL'; }
				else {$level_dbf = '';}
				
				
				
				if($kode_jab_dbf!=''){
					$ar_jab = explode('.',$kode_jab_dbf);
					$kode_digit_jab = $ar_jab[1];
				    if($kode_digit_jab>9){
						$jenis_jab = "OPERATOR";
					}else{
						$level_dbf = 'HIGH_SKILL';
						if($kode_digit_jab>=8){
							$jenis_jab = "JUNIOR SUPERVISOR";	
						}elseif($kode_digit_jab==7){
							$jenis_jab = "SUPERVISOR";	
						}elseif($kode_digit_jab==6){
							$jenis_jab = "SENIOR SUPERVISOR";	
						}elseif($kode_digit_jab==5){
							$jenis_jab = "JUNIOR MANAGER";	
						}elseif($kode_digit_jab==4){
							$jenis_jab = "SENIOR MANAGER";	
						}else{
							$jenis_jab = '';	
						}
						
					}	
				}else{
						$jenis_jab = '';
				}
				
				
				
				
				
				if($jns_kel_dbf=='L'){ $jns_kel = '1'; }else{ $jns_kel = '2';}
				
				
				if(isset($json->karyawan->$noreg_dbf)) {
        			
        				if($json->karyawan->$noreg_dbf->jenis != $jenis_jab)
					    {
					        $jenis_hasil = "Jenis Tidak Sama.";
					        $jenis_my = $json->karyawan->$noreg_dbf->jenis;
					    }else{
							$jenis_hasil = "SAMA";
							$jenis_my 	 = $json->karyawan->$noreg_dbf->jenis;
						}
						
						
        				if($json->karyawan->$noreg_dbf->tgl_masuk != $tgl_masuk_dbf)
					    {
					        $tgl_msk_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_masuk_my = $json->karyawan->$noreg_dbf->tgl_masuk;
					    }else{
							$tgl_msk_hasil = "SAMA";
							$tgl_masuk_my = $json->karyawan->$noreg_dbf->tgl_masuk;
						}
						
						if($json->karyawan->$noreg_dbf->level != $level_dbf)
					    {
					        $level_hasil = "Level Tidak Sama.";
					        $level_my = $json->karyawan->$noreg_dbf->level;
					    }else{
							$level_hasil = "SAMA";
							$level_my = $json->karyawan->$noreg_dbf->level;
						}
						
					    if($json->karyawan->$noreg_dbf->tgl_keluar != $tgl_keluar_dbf)
					    {
					        $tgl_keluar_hasil = "Tanggal Masuk Tidak Sama.";
					        $tgl_keluar_my = $json->karyawan->$noreg_dbf->tgl_keluar;
					    }else{
							$tgl_keluar_hasil = "SAMA";
							$tgl_keluar_my = $json->karyawan->$noreg_dbf->tgl_keluar;
						}
					    
					    if($json->karyawan->$noreg_dbf->nama != $nama_dbf){
							$nama_hasil    = "Nama Tidak Sama.";
							$nama_my    = $json->karyawan->$noreg_dbf->nama;
							
						    
						}else{
							$nama_hasil = "SAMA";
							$nama_my =  $json->karyawan->$noreg_dbf->nama;
						}
						
						if($json->karyawan->$noreg_dbf->nama_jabatan != $nama_jabatan_dbf ){
							$nama_jabatan_hasil = "Nama Jabatan Tidak Sama.";
							$nama_jabatan_my = $json->karyawan->$noreg_dbf->nama_jabatan;
							
						}else{
							$nama_jabatan_hasil = "SAMA";
							$nama_jabatan_my = $json->karyawan->$noreg_dbf->nama_jabatan;
						}
						
						if($json->karyawan->$noreg_dbf->grup != $grup_dbf ){
							$grup_hasil   = "Grup Tidak Sama.";
							$grup_my  	  = $json->karyawan->$noreg_dbf->grup;
							
						}else{
							$grup_hasil = "SAMA";
							$grup_my = $json->karyawan->$noreg_dbf->grup;
						}
						
						if($json->karyawan->$noreg_dbf->status_kry != $status_kry_dbf ){
							$status_kry_hasil = "Status Kry Tidak Sama.";
							$status_kry_my	  = $json->karyawan->$noreg_dbf->status_kry;
							
						}else{
							
							$status_kry_hasil = "SAMA";
							$status_kry_my = $json->karyawan->$noreg_dbf->status_kry;
						}
						
						$status_data_hasil =  'Karyawan Lama';
						
				}else{
						$jenis_hasil = $jenis_jab;
						$tgl_msk_hasil = $tgl_masuk_dbf;
						$nama_hasil = $nama_dbf;
						$nama_jabatan_hasil = $nama_jabatan_dbf;
						$grup_hasil = $grup_dbf;
						$status_kry_hasil = $status_kry_dbf;
						$status_data_hasil =  'Karyawan Baru';
						$tgl_masuk_my =  $tgl_masuk_dbf;
						$nama_my =  $nama_dbf;
						$nama_jabatan_my =  $nama_jabatan_dbf;
						$grup_my =  $grup_dbf;
						$level_my =  $level_dbf;
						$status_kry_my =  $status_kry_dbf;
						
						
				}
				
				
				
				
				$data_hasil[] = array(
        				  'no_reg' => $noreg_dbf,
        				  'no_reg_dbf' => $noreg_dbf,
        				  'cek_tgl_masuk' => $tgl_msk_hasil,
        				  'tgl_masuk_my' => $tgl_masuk_my,
        				  'tgl_masuk_dbf' => $tgl_masuk_dbf,
        				  'cek_nama' => $nama_hasil,
        				  'nama_my' => $nama_my,
        				  'nama_dbf' => $nama_dbf,
        				  'cek_nama_jabatan' => $nama_jabatan_hasil,
        				  'nama_jabatan_my' => $nama_jabatan_my,
        				  'nama_jabatan_dbf' => $nama_jabatan_dbf,
        				  'cek_grup' => $grup_hasil,
        				  'grup_my' => $grup_my,
        				  'grup_dbf' => $grup_dbf,
        				  'cek_status_kry' => $status_kry_hasil,
        				  'status_kry_my' => $status_kry_my,
        				  'status_kry_dbf' => $status_kry_dbf,
        				  'status_kry' => $status_kry_dbf,
        				  'jenis_kel' => $jns_kel,
        				  'tgl_keluar_dbf' => $tgl_keluar_dbf,
        				  'tgl_keluar_my' => $tgl_keluar_my,
        				  'cek_tgl_keluar' => $tgl_keluar_hasil,
        				  'level_dbf' => $level_dbf,
        				  'level_my' => $level_my,
        				  'cek_level' => $level_hasil,
        				  'jenis_my' => $jenis_my,
        				  'jenis_dbf' => $jenis_jab,
        				  'cek_jenis' => $jenis_hasil,
        				  'unit_dbf' => $unit_dbf,
        				  'departemen_dbf' => $departemen_dbf,
        				  'seksi_dbf' => $seksi_dbf,
        				  'divisi_dbf' => $divisi_dbf,
        				  'status_data' => $status_data_hasil
        		);
				
				    
				    
				
       }
		 
        
        
	    foreach ($data_hasil as $ayx) :
		
	      if($ayx['status_data']=='Karyawan Lama'){
	      	
	      		if($ayx['cek_tgl_masuk']!='SAMA'){
	      			$cek_reg = dtkary::find(
	      			array(
	      			'select'=> 'no_reg', 
	      			'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
	      				$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->tgl_masuk = $ayx['tgl_masuk_dbf'];
					    $update->save();
					}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
	      			
	      				
					   
					
				}
				
				if($ayx['cek_tgl_keluar']!='SAMA'){
					$cek_reg = dtkary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
						$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->tgl_keluar = $ayx['tgl_keluar_dbf'];
					    $update->save();	
					}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}			
				}
				
				if($ayx['cek_nama']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->nama = $ayx['nama_dbf'];
				    $update->save();	
				}
				
				if($ayx['cek_jenis']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->jabatan = $ayx['jenis_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_nama_jabatan']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->posisi = $ayx['nama_jabatan_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_grup']!='SAMA'){
					$cek_reg = dtkary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg>0){
		      			$update = dtkary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
					    $update->grup = $ayx['grup_dbf'];
					    $update->save();
	      			}else{
						$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
					
				}
				
				
				if($ayx['cek_level']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->skill = $ayx['level_dbf'];
				    $update->save();
				}
				
				if($ayx['cek_status_kry']!='SAMA'){
					$update = makary::find(array('conditions' => array('no_reg' => $ayx['no_reg'])));
				    $update->status_kary = $ayx['status_kry'];
				    $update->save();
				}
				
		  }
		  
		  if($ayx['status_data']=='Karyawan Baru'){
		  		
		  			$cek_reg = makary::find(
					array(
					'select'=> 'no_reg', 
					'conditions' => array('no_reg' => $ayx['no_reg'])));
	      			$count_reg = count($cek_reg);
	      			
	      			if($count_reg==0){
	      				$create_karyawan = new makary();
					    $create_karyawan->nama 					= $ayx['nama_dbf'];
					    $create_karyawan->no_reg 				= $ayx['no_reg'];
					    $create_karyawan->posisi		  		= $ayx['nama_jabatan_dbf'];
					    $create_karyawan->status_kary 			= $ayx['status_kry'];
					    $create_karyawan->skill 				= $ayx['level_dbf'];
					    $create_karyawan->jabatan 				= $ayx['jenis_dbf'];
					    $create_karyawan->unit 					= $ayx['unit_dbf'];
					    $create_karyawan->seksi 				= $ayx['seksi_dbf'];
					    $create_karyawan->dept 					= $ayx['departemen_dbf'];
					    $create_karyawan->divisi				= $ayx['divisi_dbf'];
					   // $create_karyawan->gaji_pokok 			= '0';
					    $create_karyawan->lokasi 				= 'PEKALONGAN';
					    $create_karyawan->save();
					    
					   	$create_dt_kary = new dtkary();
					    $create_dt_kary->no_reg 				= $ayx['no_reg'];
					    $create_dt_kary->kelamin 				= $ayx['jenis_kel'];
					    $create_dt_kary->tgl_masuk 				= $ayx['tgl_masuk_dbf'];
					    $create_dt_kary->tgl_keluar 			= $ayx['tgl_keluar_dbf'];
					    $create_dt_kary->grup 					= $ayx['grup_dbf'];
					    //$create_dt_kary->no_reg_dbf 			= $ayx['no_reg_dbf'];
					    $create_dt_kary->save();
					}
		  		
	      	 
			    
			   
		  	
		  }
		  
	    	
	   endforeach;
	   
        $response->status(200);
        $response->body(json_encode(array('data_karyawan' => $data_hasil),JSON_PRETTY_PRINT));
        
    } catch(PDOException $e) {
        $response->status(500);
        $response->body(json_encode(array('reasons' => $e->getMessage())));
    }
});




$app->run();